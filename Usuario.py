class Usuario:
    
    def __init__(self, nombre):
        self.nombre = nombre
        self.tareas = [] #Relacion uno a muchos con Tareas

    def agregar_tarea(self, tarea):
        self.tareas.append(tarea)

    def listar_tareas(self):
        print(f"Tareas de {self.nombre}:")
        for tarea in self.tareas:
            print(tarea)