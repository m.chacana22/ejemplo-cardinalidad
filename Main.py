from Usuario import Usuario
from Tarea import Tarea

#crear usuarios
usuario1 = Usuario("Miguel")
usuario2 = Usuario("Daniel")
usuario3 = Usuario("Daniela")

#crear tareas
tarea1 = Tarea("Hacer compras suopermercado")
tarea2 = Tarea("Pagar cuentas")
tarea3 = Tarea("Mantencion auto")

usuario1.agregar_tarea(tarea1)
usuario1.agregar_tarea(Tarea("Ir al colegio"))
usuario2.agregar_tarea(tarea2)
usuario3.agregar_tarea(tarea3)
usuario3.agregar_tarea(Tarea("Ir al colegio"))

#mostrar las tareas de los usuarios
usuario1.listar_tareas()
usuario2.listar_tareas()
usuario3.listar_tareas()

